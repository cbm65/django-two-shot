from django.shortcuts import render, get_object_or_404, redirect

from django.contrib.auth.decorators import login_required

from receipts.models import ExpenseCategory, Account, Receipt

from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


@login_required
def receipt_list(request):
  receipt_list = Receipt.objects.filter(purchaser=request.user)
  context = {
    "receipt_list": receipt_list,
  }
  return render(request, "receipts/home.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()


            return redirect("home")

    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/create.html", context)

@login_required
def category_list(request):
  category_list = ExpenseCategory.objects.filter(owner=request.user)
  context = {
    "category_list": category_list,
  }
  return render(request, "receipts/category.html", context)

@login_required
def accounts_list(request):
  accounts_list = Account.objects.filter(owner=request.user)
  context = {
    "accounts_list": accounts_list,
  }
  return render(request, "receipts/accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            ExpenseCategory = form.save(False)
            ExpenseCategory.owner = request.user
            ExpenseCategory.save()


            return redirect("category_list")

    else:
        form = ExpenseCategoryForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/categories/create.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            Account = form.save(False)
            Account.owner = request.user
            Account.save()


            return redirect("account_list")

    else:
        form = AccountForm()

    context = {
        "form": form,
    }

    return render(request, "receipts/accounts/create.html", context)
